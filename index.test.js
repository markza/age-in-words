import MockDate from 'mockdate';
import ageInWords from './index';

describe('day tests', () => {
  beforeAll(() => {
    MockDate.set('2018-04-13');
  });

  afterAll(() => {
    MockDate.reset();
  });

  test('0 days old', () => {
    const age = ageInWords('2018-04-13', { specialCases: false });
    expect(age).toBe('0 days old');
  });

  test('1 day old (singular test)', () => {
    const age = ageInWords('2018-04-12');
    expect(age).toBe('1 day old');
  });

  test('2 days old', () => {
    const age = ageInWords('2018-04-11');
    expect(age).toBe('2 days old');
  });

  test('6 days old', () => {
    const age = ageInWords('2018-04-07');
    expect(age).toBe('6 days old');
  });
});

describe('week tests', () => {
  beforeAll(() => {
    MockDate.set('2018-04-13');
  });

  afterAll(() => {
    MockDate.reset();
  });

  test('7 days old', () => {
    const age = ageInWords('2018-04-06');
    expect(age).toBe('1 week old');
  });

  test('4 weeks old', () => {
    const age = ageInWords('2018-03-13');
    expect(age).toBe('4 weeks old');
  });

  test('12 weeks and 3 days old', () => {
    const age = ageInWords('2018-01-16');
    expect(age).toBe('12 weeks old');
  });

  test('12 weeks old', () => {
    const age = ageInWords('2018-01-13');
    expect(age).toBe('12 weeks old');
  });

  test('3 months old', () => {
    const age = ageInWords('2018-01-06');
    expect(age).not.toBe('12 weeks old');
  });
});

describe('month tests', () => {
  beforeAll(() => {
    MockDate.set('2018-04-13');
  });

  afterAll(() => {
    MockDate.reset();
  });

  test('3 months old', () => {
    const age = ageInWords('2018-01-06');
    expect(age).toBe('3 months old');
  });

  test('4 months old', () => {
    const age = ageInWords('2017-12-07');
    expect(age).toBe('4 months old');
  });

  test('12 months old', () => {
    const age = ageInWords('2017-04-13', { specialCases: false });
    expect(age).toBe('12 months old');
  });

  test('13 months old', () => {
    const age = ageInWords('2017-03-13');
    expect(age).toBe('13 months old');
  });

  test('13 months and 2 days old', () => {
    const age = ageInWords('2017-03-12');
    expect(age).toBe('13 months old');
  });

  test('17 months old', () => {
    const age = ageInWords('2016-11-13');
    expect(age).toBe('17 months old');
  });

  test('18 months old', () => {
    const age = ageInWords('2016-10-13', { specialCases: false });
    expect(age).toBe('18 months old');
  });

  test('18 month and 1 day old', () => {
    const age = ageInWords('2016-10-12', { specialCases: false });
    expect(age).toBe('18 months old');
  });

  test('19 months', () => {
    const age = ageInWords('2016-09-13');
    expect(age).not.toBe('19 months old');
  });
});

describe('year and month tests', () => {
  beforeAll(() => {
    MockDate.set('2018-04-13');
  });

  afterAll(() => {
    MockDate.reset();
  });

  test('19 months old', () => {
    const age = ageInWords('2016-09-13');
    expect(age).toBe('1 year and 7 months old');
  });

  test('23 months old', () => {
    const age = ageInWords('2016-05-13');
    expect(age).toBe('1 year and 11 months old');
  });

  test('24 months old', () => {
    const age = ageInWords('2016-04-13');
    expect(age).toBe('2 years old');
  });

  test('30 months old', () => {
    const age = ageInWords('2015-10-13', { specialCases: false });
    expect(age).toBe('2 years and 6 months old');
  });

  test('36 months old', () => {
    const age = ageInWords('2015-04-13');
    expect(age).toBe('3 years old');
  });
});

describe('special case tests (default)', () => {
  beforeAll(() => {
    MockDate.set('2018-04-13');
  });

  afterAll(() => {
    MockDate.reset();
  });

  test('brand new', () => {
    const age = ageInWords('2018-04-13');
    expect(age).toBe('brand new');
  });

  test('A year old', () => {
    const age = ageInWords('2017-04-13');
    expect(age).toBe('1 year old');
  });

  test('A year and a half old', () => {
    const age = ageInWords('2016-10-12');
    expect(age).toBe('1 and a half years old');
  });

  test('2 and a half years old', () => {
    const age = ageInWords('2015-10-13');
    expect(age).toBe('2 and a half years old');
  });
});

describe('constructor tests', () => {
  beforeAll(() => {
    MockDate.set('2018-04-13');
  });

  afterAll(() => {
    MockDate.reset();
  });

  test('RFC-822', () => {
    const age = ageInWords('Wed, 20 Nov 1912 00:00:00 GMT');
    expect(age).toBe('105 years and 5 months old');
  });

  test('MM/DD/YYY', () => {
    const age = ageInWords('10/12/2016');
    expect(age).toBe('1 and a half years old');
  });

  test('YYYY/MM/DD', () => {
    const age = ageInWords('2015/10/13');
    expect(age).toBe('2 and a half years old');
  });
});
