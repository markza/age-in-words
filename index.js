const specialCases = {
  '12 months': '1 year',
  '18 months': '1 and a half years',
  '2 years and 6 months': '2 and a half years',
};

const pluralize = (val, str) => `${val} ${val > 1 ? `${str}s` : str}`;

const useDays = val => {
  if (val.week === 0 && val.month === 0 && val.year === 0) {
    return true;
  }
  return false;
};

const useWeeks = val => {
  if (val.week !== 0 && val.week < 13 && val.year === 0) {
    return true;
  }
  return false;
};

const useMonths = val => {
  if (
    (val.week >= 13 && val.year === 0) ||
    (val.month <= 6 && val.year === 1)
  ) {
    return true;
  }
  return false;
};

const specialCase = val =>
  Object.prototype.hasOwnProperty.call(specialCases, val)
    ? specialCases[val]
    : val;

const correctTimeframes = original => {
  if (useDays(original)) {
    return {
      day: original.day,
    };
  } else if (useWeeks(original)) {
    return {
      week: original.week,
    };
  } else if (useMonths(original)) {
    let { month } = original;
    if (original.year > 0) {
      month += original.year * 12;
    }
    return {
      month,
    };
  }
  const returnVal = {
    year: original.year,
  };
  if (original.month > 0) {
    returnVal.month = original.month;
  }
  return returnVal;
};

const msMinute = 60 * 1000;
const msHour = msMinute * 60;
const msDay = msHour * 24;
const msWeek = msDay * 7;
const msMonth = msDay * 30;
const msYear = msDay * 365;

module.exports = (date, opts) => {
  const options = Object.assign(
    {
      age: true,
      and: true,
      year: true,
      week: true,
      month: true,
      day: true,
      specialCases: true,
    },
    opts,
  );

  const milli = new Date(date) - new Date();
  const ms = Math.abs(milli);

  if (ms < msMinute) return options.specialCases ? 'brand new' : '0 days old';

  const timeframes = {
    year: Math.floor(ms / msYear),
    month: Math.floor((ms % msYear) / msMonth),
    week: Math.floor((ms % msYear) / msWeek),
    day: Math.floor((ms % msMonth) / msDay),
  };

  const chunks = [];
  let val;

  const parsedTimeframes = correctTimeframes(timeframes);

  Object.keys(parsedTimeframes).forEach(period => {
    val = parsedTimeframes[period];
    if (options[period]) {
      chunks.push(pluralize(val, period));
    }
  });

  // Limit the returned array to return 'max' of non-null segments
  const compiled = [];
  let i;
  const len = chunks.length;
  let limit = 0;
  const max = options.max || 10;
  for (i = 0; i < len; i += 1) {
    if (chunks[i] && limit < max) {
      limit += 1;
      compiled.push(chunks[i]);
    }
  }

  let sfx = '';
  if (milli < 0) {
    if (options.ago) {
      sfx = ' ago';
    } else if (options.age) {
      sfx = ' old';
    }
  }

  let returnAge;

  if (options.and && limit > 1) {
    if (limit === 2) {
      returnAge = compiled.join(' and ');
      if (options.specialCases) {
        returnAge = specialCase(returnAge);
      }
      return returnAge + sfx;
    }
    compiled[limit - 1] = `and ${compiled[limit - 1]}`;
  }

  returnAge = compiled.join(', ');
  if (options.specialCases) {
    returnAge = specialCase(returnAge);
  }

  return returnAge + sfx;
};
