# Age in Words

[![pipeline status](https://gitlab.com/markza/age-in-words/badges/master/pipeline.svg)](https://gitlab.com/markza/age-in-words/commits/master)

Express age in words. Accepts birthdate as constructor.


## Installation

  `yarn add age-in-words`

## Usage

  ```js
    const age = ageInWords(date, options);
  ```

  date (required):

    - Format: 'RFC-822', 'MM/DD/YYY', YYYY/MM/DD'

  options (optional):

    - `specialCases` (optional): default - true
       See specialCases.js for list of renames if enabled

## Example

```js
  const ageInWords = import 'age-in-words';
  const age = ageInWords('2016-10-12');
  console.log(age); //18 months old
```

## 

Using https://github.com/lukeed/fromNow as a starting point.
